---
title: "v02-model_calibration"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{v02-model_calibration}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>",
  fig.asp = 0.618,
  out.width = "100%",
  fig.align = "center",
  echo = FALSE
)
getFigWidth <- function(out.width = knitr::opts_chunk$get("out.width")) {
  6 * readr::parse_number(out.width) /  70
}
knitr::opts_chunk$set(
  fig.width = getFigWidth()
)

isKnit <- isTRUE(getOption('knitr.in.progress'))
```

```{r setup}
pkgload::load_all()
library(tidyverse)
library(CassiopeeR)
output_path <- "../output"
```

## Read experimental data

```{r loadData}
# Lecture des données consolidées
consolidated_file <- file.path(output_path,
                         "consolidated_data.tsv")
dfM <- readr::read_tsv(consolidated_file)
dfM <- dfM[dfM$validite, ]
dfM$Q <- dfM$`Q.(unit.=.m3/h.)`
```

## Experimental setup configuration

```{r cfg}
lCfg <- list(
  "D90" = list(ZDV = 0.12, ZT = 0.42, BT = 0.30),
  "D135" = list(ZDV = 0.20, ZT = 0.324, BT = 0.30)
)
dfM$device <- paste0("D", dfM$device_angle)
l <- lapply(setNames(nm = names(lCfg[[1]])), function(x) {
  unlist(sapply(dfM$device, function(r) lCfg[[r]][[x]]))
})
dfM <- cbind(dfM, do.call(cbind, l))
```

```{r}
equations <- c("Cassiopee v4.0.0",
               "Substracted triangles with Villemonte",
               "Bos Broad-crested with Villemonte")
```



## Creation of Cassiopee session

Create new session with a ParallelStructure module

```{r CalcFixedSetup1}
s <- Cassiopee()
uid <- addModule(s, "Structure", c(loiDebit = "TriangularTruncWeirFree"))

setAllParams <- function(df) {
  params <- c("z1", "z2", "ZDV", "ZT", "BT")
  lapply(params, function(x) SetParam(s, uid, toupper(x), df[[x]]))
  invisible()
}
```

## Compute flow with proposed equation

Configure the calculation

```{r calcDataSetup}
df <- dfM[!is.na(dfM$z1) & dfM$validite, c("exp_num", "z1", "z2", "Q", names(lCfg[[1]]), "device")]
df$z2[is.na(df$z2)] <- 0
df$z1 <- df$z1 / 1000
df$z2 <- df$z2 / 1000
df$Qobs <- df$Q / 3600

# Indicators
df$submergence <- sapply(df$z2 - df$ZDV, max, 0) / (df$z1 - df$ZDV)
df$submergence[df$submergence == 0] = NA

wet_area <- function(z) {
  triang_area <- (df$ZT - df$ZDV) * df$BT
  ifelse(z < df$ZT,
         (z - df$ZDV)^2 / (df$ZT - df$ZDV) *  df$BT,
          (df$ZT - df$ZDV) * df$BT + 2 * df$BT * (z - df$ZT))
}
df$wetarea1 <- wet_area(df$z1)
df$wetarea2 <- wet_area(df$z2)
triang_area <- (df$ZT - df$ZDV) * df$BT
df$shape_ratio <- (df$z1 - df$ZDV) / (df$ZT - df$ZDV)
```

## Calibration of free flow equations

```{r}
#' Run computation with several parameters to optimize
#' @param x Parameters [numeric] [vector] with in this order:
#' - discharge coefficient
#' - triangular equation exponent
#' - rectangular equation exponent
#' - Coefficient CdR/CdT
#' @param s Cassiopee session
#' @return Calculation result by [CassiopeeR::CalcNub]
calcWithFreeParams <- function(x, s) {
  params <- c("triang_exp",
              "rectang_exp",
              "kTR",
              "khc")
  stopifnot(length(x) == length(params) + 1)
  SetParam(s, uid, "CdT", x[1])
  lapply(seq_along(params), function(i) {
    s$eval(
      paste0("jalhyd.Session.getInstance().findNubByUid(\"", uid, "\").", params[i]," = ", x[i + 1], ";")
    )
  })
  CalcNub(s, uid)
}

costParams <- function(x, s, obs, FUN) {
  res <- FUN(x, s)
  err <- sum(((res$Q - obs) / obs)^2)
  return(err)
}
```


```{r}
plot_comp <- function(df, aes_col) {
  df$error <- (df$Qmodel - df$Qobs) / df$Qobs
  ggplot(df, aes(Qobs, error)) +
    geom_point(aes(fill = .data[[aes_col]], shape = device), size = 2) +
    scale_shape_manual(values=c(22, 25)) +
    geom_text(aes(label = exp_num), size = 2, nudge_y = 0.005) +
    labs(x = Qobs~(m^3/s), y = "Model error (%)") +
    scale_y_continuous(labels = scales::percent)
}
```


```{r}
dfFF <- df %>% filter(is.na(submergence))
setAllParams(dfFF)
x_init <- list(
  c(1, 2.5, 2.5, 1.37, 1),  # Triangle - triangle
  c(1, 2.5, 2.5, 1.37, 1),  # Triangle - triangle
  c(1, 2.5, 1.5, 0.3036013, 1.25) # Triangle -> rectangle
)
```


```{r}
oFree <- list()

optimFree <- function(iEq, FUN_EQ) {

  s$eval(
    paste0("jalhyd.Session.getInstance().findNubByUid(\"", uid, "\").num_equation = ", iEq)
  )

  (o <- optim(x_init[[iEq]],
            costParams,
            s = s,
            obs = dfFF$Qobs,
            FUN = FUN_EQ,
            method = "L-BFGS-B",
            lower = c(0.1, 1, 1, 0.01),
            upper = c(3, 3, 3, 0.6)))

  res <- FUN_EQ(o$par, s)
  dfFF$Qmodel <- res$Q
  return(list(o = o, dfFF = dfFF))
}

l <- lapply(2:3, optimFree, FUN_EQ = calcWithFreeParams)

calcPerf <- function(df) {
  data.frame(indicator = c("MAE", "RMSE", "NSE", "KGE"))
}
lapply(l, "[[", "o")
l[[3]] <- l[[2]]
l[[2]] <- l[[1]]
plot_comp(l[[2]]$dfFF, aes_col = "shape_ratio")
plot_comp(l[[3]]$dfFF, aes_col = "shape_ratio")
```

## Approach velocity correction factor

```{r}
#' Run computation using upstream head instead of water elevation
#' @param x Parameters [numeric] [vector] with in this order:
#' - discharge coefficient
#' - triangular equation exponent
#' - rectangular equation exponent
#' - Coefficient CdR/CdT
#' @param s Cassiopee session
#' @return Calculation result by [CassiopeeR::CalcNub]
calcWithCvAndFreeParams <- function(x, s) {
  setAllParams(dfFF)
  h1o <- dfFF$z1
  repeat {
    res <- calcWithFreeParams(x, s)
    h1 <- dfFF$z1 + (res$Q / dfFF$z1)^2 / (2 * 9.81)
    if (all(abs(h1 - h1o) < 1E-4) || any(h1 > 1)) break
    SetParam(s, uid, "Z1", h1)
    h1o <- h1
  }
  res
}

l <- lapply(2:3, optimFree, FUN_EQ = calcWithCvAndFreeParams)

lapply(l, "[[", "o")
l[[3]] <- l[[2]]
l[[2]] <- l[[1]]
plot_comp(l[[2]]$dfFF, aes_col = "shape_ratio")
plot_comp(l[[3]]$dfFF, aes_col = "shape_ratio")


```



## Submerged flow reduction factor

```{r}
setAllParams(df)

```


## Submerged flow without optimisation

```{r calcAndPlots, echo=FALSE, results='asis'}
setAllParams(df)

res <- lapply(seq_along(equations), function(iEq) {

  s$eval(
    paste0("jalhyd.Session.getInstance().findNubByUid(\"", uid, "\").num_equation = ", iEq)
  )
  res <- calcWithFreeParams(l[[iEq]]$o$par, s)
  o <- list(
   minimum = 0.385,
   objective = sum(((res$Q - df$Qobs) / df$Qobs)^2)
  )
  df$Qmodel <- res$Q
  equation_name <- equations[iEq]
  if (isKnit) knitr::knit_child(
    'inc/v02-equation_benchmark.Rmd', envir = environment(), quiet = TRUE
  )
})
cat(unlist(res), sep = '\n')
```


## Optimisation of Villemonte coefficient

```{r}
calcWithkV <- function(kV, s) {
  s$eval(
    paste0("jalhyd.Session.getInstance().findNubByUid(\"", uid, "\").kV = ", kV)
  )
  CalcNub(s, uid)
}

cost <- function(kV, s) {
  res <- calcWithkV(kV, s)
  err <- sum(((res$Q - df$Qobs) / df$Qobs)^2)
  return(err)
}
```


```{r, echo = FALSE, results='asis'}
optim_submerged <- function(iEq, FUN_EQ) {

  s$eval(
    paste0("jalhyd.Session.getInstance().findNubByUid(\"", uid, "\").num_equation = ", iEq)
  )
  res <- calcWithFreeParams(l[[iEq]]$o$par, s) # For setting parameters calibrated in free flow
  o <- optimise(cost, c(0.1, 1), s = s)
  equation_name <- equations[iEq]
  res <- FUN_EQ(o$minimum, s)
  df$Qmodel <- res$Q
  if (isKnit) knitr::knit_child(
    'inc/v02-equation_benchmark.Rmd', envir = environment(), quiet = TRUE
  )
}

res <- lapply(seq_along(equations), optim_submerged, FUN_EQ = calcWithkV)
cat(unlist(res), sep = '\n')
```


```{r, echo = FALSE, results='asis'}
calcwithCvAndkV <- function(kV, s) {
  setAllParams(df)
  h1 <- df$z1
  h2 <- df$z2
  repeat {
    res <- calcWithkV(kV, s)
    h1o <- h1
    h2o <- h2
    h1 <- df$z1 + (res$Q / df$z1)^2 / (2 * 9.81)
    h2[h2 > 0] <- df$z2[h2 > 0] - (res$Q[h2 > 0] / df$z2[h2o > 0])^2 / (2 * 9.81)
    if (all(abs(h1 - h1o) < 1E-4) && all(abs(h2 - h2o) < 1E-4) || any(h1 > 1)) break
    SetParam(s, uid, "Z1", h1)
    SetParam(s, uid, "Z2", h2)
  }
  res
}

res <- lapply(seq_along(equations), optim_submerged, FUN_EQ = calcwithCvAndkV)
cat(unlist(res), sep = '\n')
```

