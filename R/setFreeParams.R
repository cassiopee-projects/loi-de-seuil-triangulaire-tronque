#' Set free flow related parameters of the equations
#'
#' @param x [vector] of parameters ("triang_exp", "rectang_exp", "kATR", "khT1",
#' "khT2")
#' @param s Cassiopée session
#'
#' @return Nothing, used for side effect.
#' @export
#'
setFreeParams <- function(x, s) {
  uid <- attr(s, "uid")
  params <- c("triang_exp",
              "rectang_exp",
              "kATR",
              "khT1",
              "khT2")
  stopifnot(length(x) == length(params) + 1)
  SetParam(s, uid, "CdT", x[1])
  setNubAttributes(setNames(x[-1], params), s)
  invisible()
}
