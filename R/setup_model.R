#' Title
#'
#' @param calcType
#' @param props
#'
#' @return
#' @export
#' @import CassiopeeR
#'
#' @examples
#' s <- setup_model()
setup_model <- function(calcType = "Structure",
                        props = c(loiDebit = "TriangularTruncWeirFree")) {
  s <- Cassiopee()
  attr(s, "uid") <- addModule(s, calcType, props)
  return(s)
}
