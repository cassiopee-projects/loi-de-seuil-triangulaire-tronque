% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/setEquation.R
\name{setEquation}
\alias{setEquation}
\title{Set Equation in Cassiopee module}
\usage{
setEquation(s, iEq)
}
\arguments{
\item{s}{Cassiopee session}

\item{iEq}{Equation number (1=scw, 2=bcw, 3=war)}
}
\value{
Value returned by \code{eval} method of \code{V8} \emph{context}.
}
\description{
Set Equation in Cassiopee module
}
