% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/level_sensor_calibration.R
\name{level_sensor_calibration}
\alias{level_sensor_calibration}
\title{Sensor calibration by linear regression}
\usage{
level_sensor_calibration(df, sm, mm, daytime = NA, config = NA)
}
\arguments{
\item{df}{\link{data.frame} of measurements}

\item{sm}{Column names of sensor measurements}

\item{mm}{Column name of manual measurements}

\item{daytime}{Column name of the day time in numeric Excel format.
\code{NA} for invalidate time dependent bias correction}

\item{config}{sensors configurations (\link{character} or \link{numeric} \link{vector})}
}
\value{
A \link{list} containing:
\itemize{
\item \code{am}: \link{vector} of mean predicted values of each measurement
\item \code{mm}! \link{vector} of manual measurements
\item \code{lm}: the details of the linear regressions for each configuration of sensors
\item \code{bc}: the details of the regressions used for temporal bias correction for each configuration of sensors
\item \code{not_measured} indices where the measured values by sensors are missing and
are replaced by manual measurements.
}
}
\description{
Compute the mean predicted values of redundant sensors with bias correction
dependent in time.
}
\examples{
p <- level_sensor_calibration(data.frame(t = seq(0.1, 1, 0.1), s = seq(10), m = 2*seq(10) - 1), "s", "m", "t")
str(p)
}
