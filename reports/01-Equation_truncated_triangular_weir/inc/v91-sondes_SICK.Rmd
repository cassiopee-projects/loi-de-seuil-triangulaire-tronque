
```{r}
p <- readRDS(file.path(cache.path, sprintf("sick_sensor_calibration_%s_%sstream.RDS", expe$yymm, sonde_position)))
```

```{r, results='asis'}
if (!is.null(p$bc)) {
  dfExp <- expand.grid(sonde_num = seq(3), sonde_config = seq(length(p$bc)))
  cat(
    "#### Correction du biais temporel\n\n",
    "Les ajustements polynomiaux du biais temporel des mesures des sondes par ",
    "rapport au mesures manuelles sont représentés Figures ",
    paste0(paste(
      mapply(
        FUN = function(sonde_config, sonde_num) {
          sprintf(
            "\\@ref(fig:bias-correction-%s-%s-%i-%i)",
            expe$yymm,
            sonde_position,
            sonde_config,
            sonde_num
          )
        },
        sonde_config = dfExp$sonde_config,
        sonde_num = dfExp$sonde_num
      ),
      collapse = ", ", 
      sep = ""
    ), 
    ".\n")
  )
}
```

```{r, results='asis'}
if (!is.null(p$bc)) {
  l0 <- lapply(seq(length(p$bc)), function(sonde_config) {
    l1 <- lapply(seq(p$bc[[sonde_config]]), function(sonde_num) {
      knitr::knit(
        text = knitr::knit_expand(
          text=
"```{r {{chunk_id}}, fig.cap='{{caption}}'}
plot_sensor_bias_correction(p$bc[[sonde_config]][[sonde_num]])
```",
          caption = sprintf("Correction du biais temporel, jour %i, sonde %s n°%i (Mesures effectuées %s)", 
                            sonde_config, trad_updown[sonde_position], sonde_num, expe$desc),
          chunk_id = sprintf("bias-correction-%s-%s-%i-%i", expe$yymm, sonde_position, sonde_config, sonde_num)
        ),
        envir = environment(), 
        quiet = TRUE
      )
    })
    paste(unlist(l1), collapse = '\n')
  })
  cat(unlist(l0), sep = '\n')
}
```

#### Résultat de la calibration

```{r, results='asis'}
chunk_id <- sprintf("level-sensor-calibration-%s-%s", expe$yymm, sonde_position)
cat(sprintf("
Les écarts relatifs entre les mesures des sondes par rapports aux mesures 
manuelles sont représentés Figure \\@ref(fig:%s).\n", chunk_id))
```

```{r, results='asis'}
cat(knitr::knit(
  text = knitr::knit_expand(
    text =
"```{r {{chunk_id}}, fig.cap='{{caption}}'}
plot_level_sensor_calibration(p)
```",
caption = sprintf(
  "Résultat de la calibration, sonde %s (Mesures effectuées %s)",
  trad_updown[sonde_position],
  expe$desc
),
chunk_id = chunk_id
  ),
envir = environment(),
quiet = TRUE
))
```

