# Modélisation des seuils expérimentaux en écoulement dénoyé

```{r, include = FALSE}
source("setup.R")

df <- get_consolidated_data(output.path)
s <- setup_model()
equations <- c(
  scw = "Bos à crête mince (scw)",
  bcw = "Bos à crête épaisse (bcw)",
  war = "Réduction section mouillée (war)"
)
```

Dans ce chapitre, les 3 équations sélectionnées au chapitre précédent sont
calibrées à partir des données expérimentales correspondant au régime dénoyé.
Leurs performances respectives sont comparées afin de sélectionner l'équation
qui sera utilisée pour caler la loi d'écoulement en régime noyé.

Les paramètres des équations calibrés à ce stade sont:

- $C1$ le coefficient de débit des Équations \@ref(eq:Bos-sharp),
\@ref(eq:Bos-broad), \@ref(eq:Belaud-Kr)
- $n_T$ l'exposant utilisé dans la loi de seuil triangulaire (2,5 par défaut)
- $n_R$ l'exposant utilisé dans la loi de seuil rectangulaire (1,5 par défaut)
- $k$ le coefficient de correction appliqué à la loi pour la partie rectangulaire
(cf. Équation&nbsp;\@ref(eq:Qtransition))
- $k_{t1}$ et $k_{t2}$ les coefficients appliqués à la hauteur du triangle $H_B$
pour calculer respectivement le début et la fin de la transition entre
l'équation triangulaire et l'équation rectangulaire
(cf. Figure \@ref(fig:decision-a))

## Sans prise en compte de la vitesse d'approche

```{r}
#' Run computation with several parameters to optimize
#' @param x Parameters [numeric] [vector] with in this order:
#' - discharge coefficient
#' - triangular equation exponent
#' - rectangular equation exponent
#' - adjustement coefficient for rectangular flow
#' - minimum height of transition between triangular and rectangular flow
#' - maximum height of transition between triangular and rectangular flow
#' @param s Cassiopee session
#' @return Calculation result by [CassiopeeR::CalcNub]
calcWithFreeParams <- function(x, s) {
  setFreeParams(x, s)
  CalcNub(s, attr(s, "uid"))
}

x_init <- setNames(
  rep(
    list(
      #   Cd, expT, expR, kATR, khT1, khT2
      c(1.37,  2.5,  1.5,    1,    1,  5/4))
    , 3),
  c(names(cfg$models))
)

x_bounds <- list(
  lower =
    c(0.1,   1,    1,  0.8,  1.0,  1.0),
  upper =
    c(3,     3,    3,  2.0,  2.0,  3.0)
)

test_calc <- function(iEq) {
  setEquation(s, iEq)
  calcWithFreeParams(x_init[[iEq]], s)
}
#
# dfTest <- do.call(cbind, lapply(seq(3), test_calc))

costParams <- function(x, s, obs, FUN_EQ, FUN_TRANSFO = identity) {
  res <- FUN_EQ(x, s)
  mod <- FUN_TRANSFO(res$Q)
  obs2 <- FUN_TRANSFO(sapply(obs, max, 1E-7))
  nrmse <- sqrt(sum(((mod - obs2))^2) / length(obs)) / mean(obs2)
  mae <- mean(abs(mod - obs2) / obs2) # For testing but worse results on MAE at the end!
  if (is.na(nrmse)) browser()
  return(nrmse)
}
```


```{r}
dfFF <- df %>% filter(is.na(submergence))
setAllParams(s, dfFF)
```


```{r}
oFree <- list()

optimFree <- function(iEq, s, FUN_EQ, FUN_TRANSFO = identity, x_init, x_bounds) {
  setEquation(s, iEq)
  o <- optim(x_init[[iEq]],
             costParams,
             s = s,
             obs = dfFF$Qobs,
             FUN_EQ = FUN_EQ,
             FUN_TRANSFO = FUN_TRANSFO,
             method = "L-BFGS-B",
             lower = x_bounds$lower,
             upper = x_bounds$upper)

  res <- FUN_EQ(o$par, s)
  dfFF$Qmodel <- res$Q
  return(list(o = o, dfFF = dfFF))
}

boxcox_factory <- function(lambda) {
  function(x) {
    forecast::BoxCox(x, lambda = lambda)
  }
}

boxcox <- boxcox_factory(forecast::BoxCox.lambda(dfFF$Qobs))
power02 <- function(x) x^0.2
powerminus05 <- function(x) x^(-0.5)
```


```{r}
format_opt_result <- function(x) {
  o <- x$o
  setNames(
    c(o$par, o$value * 100),
    c("C1", "triangular exp.", "rectangular exp.", "k", "kht1", "kht2", "NRMSE (%)")
  )
}

calFF <- function(s, FUN_OPTIM, FUN_EQ, FUN_TRANSFO, x_init, x_bounds) {
  l <- lapply(setNames(1:3, names(cfg$models)),
              FUN_OPTIM,
              s = s,
              FUN_EQ = FUN_EQ,
              FUN_TRANSFO = FUN_TRANSFO,
              x_init = x_init,
              x_bounds = x_bounds)
  df_opt_res <- as.data.frame(do.call(rbind, lapply(l, format_opt_result)))
  dfModel <- do.call(
    rbind,
    lapply(names(l), function(x) {
      df <- l[[x]]$dfFF
      df$model <- cfg$models[[x]]
      len_model <- nchar(cfg$models[[x]])
      df$model_short <- str_sub(cfg$models[[x]], len_model - 3, len_model - 1)
      df
    })
  ) %>% arrange(model, h1)
  return(list(opt = df_opt_res, dfModel = dfModel))
}
```

```{r}
lh1 <- calFF(s, optimFree, calcWithFreeParams, identity, x_init, x_bounds)
```

```{r}
#| echo = FALSE,
#| results = "asis"
conf_id = "EcoulDenoye"
conf_name = "Ecoulement dénoyé sans prise en compte de la vitesse à l'amont:"
l <- lh1
if (isKnit) src <-
  knitr::knit_expand('inc/03-01-benchmark_free_flow.Rmd', envir = environment(), quiet = TRUE)
```

Le résultat de l'optimisation des paramètres est présentée dans le Tableau
\@ref(tab:EcoulDenoyeFreeFlowParamCalib).

Les Figures \@ref(fig:EcoulDenoyeFreeFlowModelD90) et
\@ref(fig:EcoulDenoyeFreeFlowErrorModelD90) présentent respectivement les courbes ajustées
des lois de débits et les erreurs relatives de chacun des
modèles pour les 3 équations pour le seuil D90.
Les mêmes éléments sont présentés dans les Figures \@ref(fig:EcoulDenoyeFreeFlowModelD135) et
\@ref(fig:EcoulDenoyeFreeFlowErrorModelD135) pour le seuil D135.

La dispersion des points aux très faibles hauteurs d'eau peut s'expliquer par
la géométrie du seuil qui contient un léger arrondi à la pointe du triangle,
la difficulté d'avoir une mesure de débit voire un débit stable sur cette installation
sur les très faibles débits et une très grande sensibilité de l'erreur relative
à faible débit par rapport à des débits plus élevés.

Les symboles représentés en rouge sont ceux correspondants aux observations où le
niveau d'eau amont ne dépasse pas la partie triangulaire du seuil.
Sur le seuil D90, on observe un décrochage net entre les écoulements ayant
uniquement lieu sur la partie triangulaire et les autres quelque soit l'équation
utilisée.
Ce peut être due à la sur-représentation des observations sur le seuil D135 qui
représente un poids plus important dans l'optimisation des paramètres et pour
lequel ce décrochage est moins net.

La Figure \@ref(fig:EcoulDenoyeFreeFlowErrorModels) montre que la valeur minimale
de l'erreur moyenne absolue (MAE) est atteinte pour l'équation de Bos à crête
épaisse (*bcw*) (cf. Équation&nbsp;\@ref(eq:Bos-broad)) avec une erreur moyenne de 1,9 %.
L'équation Bos à crête mince (*scw*) (cf. Équation\@ref(eq:Bos-sharp)) la devance
légèrement pour le seuil D90 avec une erreur moyenne de 1,8 % mais les résultats
sont très dégradés pour le seuil D135 avec 3 % d'erreur.
L'erreur du modèle pour les deux types de seuils (D90 et D135) avec l'équation
de Bos à crête épaisse (*bcw*) sont présentés à la Figure
\@ref(fig:EcoulDenoyeFreeFlowErrorModelBcw).

`r knitr::knit(text = unlist(src))`

## avec prise en compte de la vitesse d'approche

Les équations théoriques font intervenir la charge $H_1$ et non pas la hauteur
d'eau $h_1$ ce qui est problématique car la charge $H_1$ dépend du débit.

Le coefficient de vitesse d'approche $C_v$ permet de prendre en compte l'écart
attendu de la prise en compte de la charge $H_1$ tout en utilisant la hauteur
d'eau $h_1$ dans l'équation de débit (Voir @bosDischargeMeasurementStructures1989 p.30).

La valeur théorique du coefficient d'approche s'obtient à l'aide de la formule
$C_v = (H_1 / h_1)^n$ avec $n$ l'exposant utilisé dans la loi de débit. En faisant
l'approximation $n=2.5$ pour le seuil D90 et $n=1.5$ pour le seuil D135,
on obtient les valeurs de $C_v$ de la Figure \@ref(fig:Cv-fn-h1).

(ref:Cv-fn-h1) Evolution du coefficient de vitesse d'approche $C_v$ en fonction de la hauteur d'eau amont ($h_1$)

```{r Cv-fn-h1, fig.cap='(ref:Cv-fn-h1)'}
dfFF$H1 <- dfFF$h1 + (dfFF$Qobs / (dfFF$z1))^2 / (2*9.81)
dfFF$Cv <- (dfFF$H1 / dfFF$h1)^2.5
dfFF$Cv[dfFF$device == "D135"] <-
  (dfFF$H1[dfFF$device == "D135"] / dfFF$h1[dfFF$device == "D135"])^1.5
# dfFF$Cv[is.na(dfFF$shape_ratio)] <-
#   (dfFF$H1[is.na(dfFF$shape_ratio)] / dfFF$h1[is.na(dfFF$shape_ratio)])^2.5
ggplot(dfFF, aes(x = h1, y = Cv, shape = device)) +
  geom_point() +
  scale_shape_manual(values=c(22, 25)) +
  labs(x = "h1 (m)",
       shape = "Modèle de seuil")
```


Cette substitution peut se traduire par l’Équation&nbsp;\@ref(eq:QCv):

\begin{equation}
  Q(H_1) = C_v\ Q(h_1)
  (\#eq:QCv)
\end{equation}

Avec $H_1 = h_1 + V^2/2g$, $V$ étant la vitesse d'approche dans le chenal à
l'amont.

La difficulté réside dans le fait que la charge dépend de la vitesse ($V = Q/S$)
et donc du débit qui est la valeur que l'on cherche à calculer. Ce problème est résolu
itérativement en posant la suite \@ref(eq:QH1) qui s'arrête une fois que le calcul
du débit a convergé:

\begin{equation}
\begin{aligned}
  Q(1) = Q(h_1) \\
  Q(n) = Q(H_{1(n-1)})
\end{aligned}
  (\#eq:QH1)
\end{equation}

```{r}
#' Run computation using upstream head instead of water elevation
#' @param x Parameters [numeric] [vector] with in this order:
#' - discharge coefficient
#' - triangular equation exponent
#' - rectangular equation exponent
#' - Coefficient CdR/CdT
#' @param s Cassiopee session
#' @return Calculation result by [CassiopeeR::CalcNub]
calcWithCvAndFreeParams <- function(x, s) {
  setAllParams(s, dfFF)
  H1o <- dfFF$z1
  repeat {
    res <- calcWithFreeParams(x, s)
    H1 <- dfFF$z1 + (res$Q / dfFF$z1)^2 / (2 * 9.81)
    if (all(abs(H1 - H1o) < 1E-4) || any(H1 > 1)) break
    SetParam(s, attr(s, "uid"), "Z1", H1)
    H1o <- H1
  }
  res
}
```


```{r}
l <- calFF(s, optimFree, calcWithCvAndFreeParams, identity, x_init, x_bounds)
```

```{r}
#| echo = FALSE,
#| results = "asis"
conf_id <- "EcoulDenoyeCv"
conf_name = "Ecoulement dénoyé avec prise en compte de la vitesse à l'amont:"
if (isKnit) src <-
  knitr::knit_expand('inc/03-01-benchmark_free_flow.Rmd', envir = environment(), quiet = TRUE)
```

Le résultat de l'optimisation des paramètres est présenté dans le Tableau
\@ref(tab:EcoulDenoyeCvFreeFlowParamCalib).

Les Figures \@ref(fig:EcoulDenoyeCvFreeFlowModelD90) et
\@ref(fig:EcoulDenoyeCvFreeFlowErrorModelD90) présentent respectivement les courbes ajustées
des lois de débits et les erreurs relatives de chacun des
modèles pour les 3 équations pour le seuil D90.
Les mêmes élements sont présentés dans les Figures \@ref(fig:EcoulDenoyeCvFreeFlowModelD135) et
\@ref(fig:EcoulDenoyeCvFreeFlowErrorModelD135) pour le seuil D135.

La Figure \@ref(fig:EcoulDenoyeCvFreeFlowErrorModels) montre ici aussi que la valeur
minimale de l'erreur moyenne absolue (MAE) est atteinte avec l'équation de Bos à crête
épaisse (*bcw*) (cf. Équation&nbsp;\@ref(eq:Bos-broad)) avec des erreurs inférieures à
2 % pour les deux types de seuils (D90 et D135).
L'erreur du modèle pour les deux types de seuils avec l'équation
de Bos à crête épaisse (*bcw*) sont présentés à la Figure
\@ref(fig:EcoulDenoyeCvFreeFlowErrorModelBcw).

`r knitr::knit(text = unlist(src))`

## Conclusion

L'équation de Bos à crête épaisse (*bcw*) (Équation&nbsp;\@ref(eq:Bos-broad-C1))
devance les deux autres équations et est donc sélectionnée pour étudier la loi
de débit en écoulement noyé.

L'introduction du coefficient de vitesse d'approche $C_v$ n'apporte pas de gain
significatif (0,1 % en erreur moyenne).
De plus, la vitesse d'approche n'est pas utilisée dans les équations des passes à
poisson dans Cassiopée, par soucis d'homogénéité, ce coefficient ne sera pas
considéré dans la suite de ce rapport.

```{r, include = FALSE}
iEq <- 2
saveRDS(list(iEq = iEq, params = lh1$opt[iEq, -7]), file.path(cache.path, "CalibrationFreeFlow.RDS"))
```
