# Modélisation de la loi de débit en écoulement noyé

```{r, include = FALSE}
source("setup.R")

df <- get_consolidated_data(output.path)
l_ff_calib <- readRDS(file.path(cache.path, "CalibrationFreeFlow.RDS"))
s <- setup_model()
setEquation(s, l_ff_calib$iEq)
setFreeParams(l_ff_calib$params, s)
```


## Détermination du coefficient de Villemonte

L'exposant $m$ de l'Équation&nbsp;\@ref(eq:villemonte-Qgen) peut être déterminé
par régression linéaire en passant par les logarithmes&nbsp;:

\begin{equation}
  \ln \left( \dfrac{Q}{Q1} \right) = m \ln \left( 1 - \dfrac{Q_2}{Q_1} \right)
  (\#eq:villemonte-Qgen-ln)
\end{equation}

Avec $Q$, le débit observé, et $Q_1$ (resp. $Q_2$) le débit en écoulement dénoyé
calculé  à partir de la cote de l'eau amont (resp. aval). Le résultat graphique
de cette régression est représenté Figure&nbsp;\@ref(fig:regressionVillemonte).

```{r dfSubm}
dfSubm <- df[!is.na(df$submergence), ]
dfQ1 <- dfSubm %>% mutate(z2 = 0)
setAllParams(s, dfQ1)
resQ1 <- CalcNub(s, attr(s, "uid"))
dfQ2 <- dfSubm %>% mutate(z1 = z2, z2 = 0)
setAllParams(s, dfQ2)
resQ2 <- CalcNub(s, attr(s, "uid"))

dfSubm <- cbind(dfSubm, Q1 = resQ1$Q, Q2 = resQ2$Q)
dfSubm <- dfSubm %>% mutate(x = 1 - Q2/Q1, y = Qobs/Q1)
```

```{r}
#| regressionVillemonte,
#| fig.cap = "Régression linéaire de $Q_{obs}/Q_1$ fonction de $1-Q_2/Q_1$ (échelles logarithmiques)"
library(ggpmisc)
ggplot(dfSubm, aes(x = x, y = y)) +
  scale_x_continuous(trans = scales::log_trans(),
                     breaks = scales::log_breaks()) +
  scale_y_continuous(trans = scales::log_trans(),
                     breaks = scales::log_breaks()) +
  stat_poly_line() +
  stat_poly_eq(use_label(c("eq", "R2"))) +
  geom_point() +
  labs(x = expression(1-Q[2]/Q[1]),
       y = expression(Q[obs]/Q[1]))
```

```{r lmSubm}
m <- lm(log(y) ~ log(x), dfSubm)
kV = round(m$coefficients[2], 3)
```

Le coefficient $m$ est ici égal à `r kV`, avec un coefficient de détermination de
0.95. Une valeur
assez proche de $m=0.385$ déterminée par @villemonteSubmergedWeirDischarge1947 pour
toutes les formes de déversoirs.

(ref:plotlmDevices) Régressions linéaires par type de seuil de $Q_{obs}/Q_1$ fonction de $1-Q_2/Q_1$ (échelles logarithmiques)

```{r plotlmDevices}
#| plotlmDevices,
#| out.width = "90%",
#| fig.width = getFigWidth("90%"),
#| fig.cap="(ref:plotlmDevices)"
library(ggpmisc)
ggplot(dfSubm, aes(x = x, y = y, shape = device)) +
  scale_x_continuous(trans = scales::log_trans(),
                     breaks = scales::log_breaks()) +
  scale_y_continuous(trans = scales::log_trans(),
                     breaks = scales::log_breaks()) +
  scale_shape_manual(values=c(22, 25)) +
  stat_poly_line(aes(col = device)) +
  stat_poly_eq(use_label(c("eq", "R2"))) +
  geom_point() +
  labs(x = expression(1-Q[2]/Q[1]),
       y = expression(Q[obs]/Q[1]))
```

Les régressions effectuées séparément pour les deux types de déversoirs représentées
Figure \@ref(fig:plotlmDevices) donnent des valeurs de $m$ de 0,326
et 0,353 pour respectivement le seuil D135 et D90.
Etant donné ces faibles différences et la valeur très élevée du coefficient de
détermination pour la régression réalisée globalement Figure \@ref(fig:regressionVillemonte),
on peut considérer que l'utilisation d'un coefficient $m$ global est suffisamment
robuste pour ne pas avoir à le différencier en fonction du type de seuil.

```{r}
calcWithkVkI <- function(kVkI, s) {
  setNubAttributes(kVkI, s)
  CalcNub(s, attr(s, "uid"))
}
```

## Résultat du modèle d'écoulement noyé avec la formulation classique de Villemonte

```{r}
calcAndPlotKr <- function(dfSubm, kVkI, mae_quantiles = seq(0, 0.9, 0.1)) {
  setAllParams(s, dfSubm)
  dfSubm <- dfSubm %>% mutate(Qmodel = calcWithkVkI(kVkI, s)$Q,
                              Obs = Qobs / Q1,
                              Model = Qmodel / Q1)
  dfSubm$err <- (dfSubm$Qmodel - dfSubm$Qobs) / dfSubm$Qobs
  pFit <- ggplot(
    dfSubm %>%
      select(submergence, Obs, Model, device) %>%
      pivot_longer(
        -c(submergence, device),
        names_to = "Source",
        values_to = "Kr"
      ),
    aes(submergence, Kr, col = Source, shape = device)
  ) +
    geom_point() +
    scale_x_continuous(breaks = seq(0, 1, 0.1)) +
    scale_y_continuous(breaks = seq(0, 1, 0.1)) +
    scale_shape_manual(values = c(22, 25)) +
    labs(x = bquote(h[2] / h[1]), y = bquote(Q/Q[1]))
  pErr <- ggplot(dfSubm, aes(submergence, err, fill = Qmodel)) +
    geom_point(aes(shape = device), size = 2) +
    scale_shape_manual(values = c(22, 25)) +
    labs(x = bquote(h[2] / h[1]),
         y = "erreur sur Q (%)",
         fill = bquote('Débit simulé '(m ^ 3 / s))) +
    scale_y_continuous(labels = scales::percent) +
    scale_fill_viridis_c() +
    geom_text_repel(aes(y = err, label = exp_num),
                    size = 2,
                    nudge_y = 0.005)

  dfAE <- dfSubm |> mutate(AE = abs(err)) |> mutate(bins = cut(submergence, breaks = seq(0,1,0.1)))

  pAE <- ggplot(dfAE, aes(x = bins, y = AE)) + geom_boxplot() +
    scale_y_continuous(labels = scales::percent) +
    stat_summary(fun.y=mean, geom="point", shape=23, size=2, fill = "red") +
    labs(x = bquote(h[2] / h[1]), y = "Erreur absolue (%)")
  return(list(pFit = pFit, pErr = pErr, dfSubm = dfSubm, AE = dfAE, pAE = pAE))
}
```


La Figure \@ref(fig:VillemonteClassiqueKr) montre que le modèle sous estime le
coefficient de réduction de débit pour les ennoiements situés entre 20 et 85%.

(ref:VillemonteClassiqueKr) Comparaison des valeurs de $Q/Q_1$ observées et simulées avec la formulation classique de Villemonte

```{r}
#| VillemonteClassiqueKr,
#| out.width = "90%",
#| fig.width = getFigWidth("90%"),
#| fig.cap="(ref:VillemonteClassiqueKr)"
l <- calcAndPlotKr(dfSubm, c(kV = unname(kV), kI = 1))
l$pFit
```

La Figure \@ref(fig:VillemonteClassiqueError) montre que les erreurs sont
systématiquement inférieures à 10 % pour les ennoiements inférieurs à 50 % avec
une tendance moyenne à sous-estimer le coefficient de réduction de débit pour
les ennoiements supérieurs à 25 %.
La distribution des erreurs absolues Figure \@ref(fig:VillemonteClassiqueMAE)
montre qu'au moins 75 % des erreurs restent inférieures à 10 % sur toute la gamme des
ennoiements.

(ref:VillemonteClassiqueError) Erreur relative du modèle noyée avec la formulation classique de Villemonte en fonction du coefficient d'ennoiement

```{r}
#| VillemonteClassiqueError,
#| out.width = "90%",
#| fig.width = getFigWidth("90%"),
#| fig.cap="(ref:VillemonteClassiqueError)"
l$pErr
```

(ref:VillemonteClassiqueMAE) Distribution de l'Erreur relative du modèle noyée avec la formulation classique de Villemonte en fonction du coefficient d'ennoiement (moyenne en rouge)

```{r}
#| VillemonteClassiqueMAE,
#| out.width = "90%",
#| fig.width = getFigWidth("90%"),
#| fig.cap="(ref:VillemonteClassiqueMAE)"
l$pAE
```


## Résultat du modèle d'écoulement noyé avec la formulation modifiée de Villemonte {#ch:resultat-villemonte-modif}

La régression permettant d'obtenir le coefficient de Villemonte
Figure&nbsp;\@ref(fig:regressionVillemonte) fait apparaître une ordonnée à l'origine
non nulle.

L’Équation&nbsp;\@ref(eq:villemonte-Qgen-ln) peut être complétée par l'ordonnée à l'origine
$o$ comme suit:

\begin{equation}
  \ln \left( \dfrac{Q}{Q1} \right) = m \ln \left( 1 - \dfrac{Q_2}{Q_1} \right) + \ln (o)
  (\#eq:villemonte-Qgen-ln-modif)
\end{equation}

L'Équation  \@ref(eq:villemonte-Qgen) devient alors:

\begin{equation}
  Q = Q_1 \left [ 1 - \dfrac{Q_2}{Q_1} \right ]^m \times k_I
  (\#eq:villemonte-Qgen-modif0)
\end{equation}

Avec $k_I = \exp(o)$.

Il faut cependant que $k_I$ soit écrêté à $1$ pour assurer la continuité des débits
avec le régime dénoyé. On obtient finalement l'Equation \@ref(eq:villemonte-Qgen-modif2).

\begin{equation}
  Q = Q_1 \min\left(1, \left [ 1 - \dfrac{Q_2}{Q_1} \right ]^m \times k_I\right)
  (\#eq:villemonte-Qgen-modif2)
\end{equation}

```{r}
kI <- exp(m$coefficients[1])
```

La régression nous donne l'application numérique suivante:

\begin{equation}
  Q = Q_1 \min\left(1, \left [ 1 - \dfrac{Q_2}{Q_1} \right ]^{`r round(kV, 3)`} \times `r round(kI, 3)`\right)
  (\#eq:villemonte-Qgen-modif-num)
\end{equation}

La formule modifiée (Figure \@ref(fig:VillemonteModifKr)) corrige la sous-estimation
observée Figure \@ref(fig:VillemonteClassiqueKr).
Pour les ennoiements inférieurs à 40 %, le modèle n'autorise pas de coefficient
de débit supérieur à 1 contrairement à certaines observations.
L'erreur en fonction de l'ennoiement Figure \@ref(fig:VillemonteModifError)
montre que le biais observé Figure \@ref(fig:VillemonteClassiqueError) a
disparu.
La distribution des erreurs absolues Figure \@ref(fig:VillemonteModifMAE) montre
que la quasi totalité des erreurs restent sous la barre des 5 % pour tous les
ennoiements inférieurs à 80 %.
La moyenne des erreurs dépasse tout juste 5 % pour les ennoiements compris entre
80 et 90 %, cela dit la moyenne est tirée vers le haut par les quelques erreurs
extrêmes certainement dues à l'incertitude des observations.
Le 3^ème^ quartile des erreurs reste très inférieur à 5 %.
Pour les ennoiements supérieurs à 90 %, la nouvelle formulation dégrade le résultat
mais le 3ème quartile des erreurs reste inférieur à 10 % ce qui reste honorable
étant donné les difficultés à estimer les débits sur les forts ennoiements.

(ref:VillemonteModifKr) Comparaison des valeurs de $Q/Q1$ observées et simulées avec la formulation modifiée de Villemonte

```{r}
#| VillemonteModifKr,
#| out.width = "90%",
#| fig.width = getFigWidth("90%"),
#| fig.cap="(ref:VillemonteModifKr)"
l <- calcAndPlotKr(dfSubm, kVkI = c(kV = unname(kV), kI = unname(kI)))
l$pFit
```

(ref:VillemonteModifError) Erreur relative du modèle noyée avec la formulation classique de Villemonte

```{r}
#| VillemonteModifError,
#| out.width = "90%",
#| fig.width = getFigWidth("90%"),
#| fig.cap="(ref:VillemonteModifError)"
l$pErr
```

(ref:VillemonteModifMAE) Distribution de l'erreur relative du modèle noyée avec la formulation modifiée de Villemonte en fonction du coefficient d'ennoiement (moyenne en rouge)

```{r}
#| VillemonteModifMAE,
#| out.width = "90%",
#| fig.width = getFigWidth("90%"),
#| fig.cap="(ref:VillemonteModifMAE)"
l$pAE
```
