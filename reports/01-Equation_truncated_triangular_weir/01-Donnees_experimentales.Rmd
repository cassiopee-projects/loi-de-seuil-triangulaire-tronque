# Données expérimentales

```{r setup, include = FALSE}
source("setup.R")
```

## Matériel expérimental

Les expériences ont été conduites à la halle hydraulique de l'Institut Agro
de Montpellier sur le canal de l'espace Boussinesq.

### Caractéristiques du canal 

Ce canal est un canal vitré à fond plat de 12 mètres de longueur et 1 mètre de 
large (Figure \@ref(fig:schema-canal-iam))

```{r schema-canal-iam, fig.cap="Schéma du canal espace Boussinesq de la halle hydraulique de l'Institut Agro à Montpellier"}
knitr::include_graphics("img/schema_canal_iam.png")
```

Le canal est équipé de 3 pompes à variateur, 2 pompes d'une capacité de 75&nbsp;L/s
et une pompe de 1,1&nbsp;kW pour les petits débits.
La combinaison des trois pompes permet d'obtenir n'importe quel débit entre 
6&nbsp;L/s et 165&nbsp;L/s.

L'exutoire du canal est équipé d'un seuil motorisé permettant de faire varier 
la hauteur d'eau à l'aval.

La mesure de débit est réalisée par un débitmètre électromagnétique 
KROHNE WATERFLUX 3300 F permettant une mesure des débits entre 1,1&nbsp;L/s et
333&nbsp;L/s avec une précision annoncée de 0,2 %.
La mesure du débit se fait par une lecture visuelle du débit instantané sur un 
afficheur dédié. Cependant, pour les besoins de l'expérience, une installation
comprenant une caméra et un enregistrement continu au pas de temps de la seconde 
des débits permettant d'apprécier le débit moyen à 30 secondes ou une minute
a permis de réaliser des mesures de débit avec une appréciation de l'incertitude 
due aux légères variations de débit des pompes autour de leur point de consigne 
(Figure \@ref(fig:photo-mesure-debit)).

```{r photo-mesure-debit, out.width="100%",fig.cap="Photos de l'installation de mesure des débits par photographie et reconnaissance de caractères"}
knitr::include_graphics("img/photo_mesure_debit.png")
```

Les hauteurs d'eau ont été mesurées à une distance d'environ 50 cm à l'amont du 
seuil et à 3,30 m à l'aval du seuil. Ces mesures ont été réalisées parallèlement
à la règle et à l'aide de 3 sondes à ultrason sur chaque localisation de mesure.
Le détail du calage des sondes à ultrason figure en annexe.
Les hauteurs sont mesurées par rapport au fond du canal. Dans la suite du rapport, 
ne figurent que les hauteurs d'eau amont $h_1$ et aval $h_2$ déduction faite de 
la hauteur de pelle du seuil utilisé.

### Caractéristiques des seuils

Les seuils ont été réalisés sur mesure en PMMA (poly méthacrylate de méthyle
acrylique) plus couramment appelé plexiglass de 19 mm d'épaisseur par découpe 
laser. Les deux seuils ont les bords biseautés à 45°.

Le premier seuil doté d'un angle de sommet de triangle de 90° est appelé dans la
suite modèle D90 (Photo Figure \@ref(fig:photo-D90)).
Le second seuil avec un angle de sommet de triangle de 135° est appelé dans la 
suite modèle D135 (Voir photo Figure \@ref(fig:photo-D135) et plan coté de
fabrication Figure \@ref(fig:plan-D135)).

```{r photo-D90, out.width="50%",fig.cap="Photos du seuil modèle D90 installé dans le canal, vue depuis l'aval"}
knitr::include_graphics("img/photo_seuil_D90.jpg")
```

```{r plan-D135, out.width="50%",fig.cap="Plan coté  de conception du seuil modèle D135"}
knitr::include_graphics("img/plan_seuil_D135.png")
```

```{r photo-D135, out.width="50%",fig.cap="Photos du seuil modèle D135 installé dans le canal, vue en fonctionnement depuis l'aval"}
knitr::include_graphics("img/photo_seuil_D135.jpg")
```

Les cotes des deux seuils conformément aux notations du schéma
Figure&nbsp;\@ref(fig:Bos-section) 
sont détaillées dans le Tableau&nbsp;\@ref(tab:dimensions-seuils).

Table: (\#tab:dimensions-seuils) Dimensions des seuils modèle D90 et D135

Caractéristique                      Modèle D90      Modèle D135
------------------------            ------------    -------------
Pelle (mm)                                  120              200
Demi-angle $\alpha$ (°)                      45             67.5
Largeur du seuil $B_C$ (mm)                 600              600
Hauteur du triangle $H_B$ (mm)              300              124


## Jeux de données récoltés

Les mesures sur le seuil D90 ont été réalisées du 24 au 26 juillet 2023.
Les mesures sur le seuil D135 ont été réalisés les 27 et 28 novembre 2023 et 
un complément de mesures en régime noyé a été effectué le 23 juillet 2024.

```{r, include = FALSE}
# Journal des évènements
df_events <- read_event_journal(path = input.path, fields = c("a", "Q"))

# Traitement des données "manuelles"
## Mesures effectuées du 24 au 26 juillet 2023 avec le seuil à 90°
# Lecture des données dans le fichier Excel
manual.file <- file.path(input.path,
                         "20230724_Halle_hydr_TriangTonque.xlsx")
sheets <- openxlsx::getSheetNames(manual.file)
dfM <- openxlsx::read.xlsx(manual.file,
                    sheet = "Mesures",
                    startRow = 7)
duplicated_cols <- c(6, 11)
names(dfM)[duplicated_cols] <- paste0(names(dfM)[duplicated_cols], c(".h1", ".h2"))

#Fusion des mesures manuelles avec le journal d'expériences
marginQ <- 0.25
names(dfM)[names(dfM) == "Q.(unit.=.m3/h.)"] <- "Qm3h"
dfM$Qmin <- dfM$Qm3h * (1 - marginQ)
dfM$Qmax <- dfM$Qm3h * (1 + marginQ)
dfM$event <- 0
dfM$dt <- as.POSIXct(NA)
iE = 0
for (iM in 1:nrow(dfM)) {
  rM <- dfM[iM,]
  repeat {
    iE <- iE + 1
    if (iE > nrow(df_events) || iE - max(dfM$event) > 50) {
      iE <- max(dfM$event)
      break
    }
    rE <- df_events[iE, ]
    if (rE$Q > rM$Qmin && rE$Q < rM$Qmax && (is.na(rM$angle) || rM$angle == rE$a)) {
      dfM$event[iM] <- iE
      dfM$dt[iM] <- rE$dt
      break
    }
  }
}

## Mesures effectuées les 27-28 novembre 2023 avec le seuil à 135°

# Lecture des données dans le fichier Excel
manual.file <- file.path(input.path,
                         "20231128_mesures_triangTronque.xlsx")
sheets <- openxlsx::getSheetNames(manual.file)
dfFreeFlow <- openxlsx::read.xlsx(manual.file,
                                  sheet = "Tout",
                                  cols = 2:3,
                                  rows = 2:60)
names(dfFreeFlow) <- c("Qm3h",
                       "Niveau.amont.h1.(unit.=.mm.)-1")
dfSubmerged <- openxlsx::read.xlsx(manual.file,
                                  sheet = "Tout",
                                  cols = 7:10,
                                  rows = 2:41)
dfSubmerged$`débit.(m3/s)` <- NULL
names(dfSubmerged) <- c(names(dfFreeFlow), "Niveau.aval.h2-1")

rbind.all.columns <- function(x, y) {

    x.diff <- setdiff(colnames(x), colnames(y))
    y.diff <- setdiff(colnames(y), colnames(x))

    x[, c(as.character(y.diff))] <- NA

    y[, c(as.character(x.diff))] <- NA

    return(rbind(x, y))
}
df135 <- rbind.all.columns(dfFreeFlow, dfSubmerged)
df135$validite <- TRUE
```

```{r}
## Mesures effectuées le 24 juillet 2024 avec le seuil à 135°

# Lecture des données dans le fichier Excel
manual.file <- file.path(input.path,
                         "20240723_mesures_triangTronque.xlsx")
sheets <- openxlsx::getSheetNames(manual.file)
df2407 <- openxlsx::read.xlsx(manual.file,
                                  sheet = "Mesures",
                                  cols = 1:14,
                                  rows = 1:131)
names(df2407)[grepl("Débit\\.moyen", names(df2407))] <- "Qm3h"
df2407$validite <- TRUE
```


```{r, include = FALSE}
## Calibration des capteurs de niveau

## Mesures du 24 au 26 juillet 2023 avec le seuil à 90°

# Niveau amont
z1 <- level_sensor_calibration(dfM,
  names(dfM)[grep("Niveau.amont.h1", names(dfM), fixed = TRUE)],
  "Niveau.mesuré.règle.(mm)",
  "heure"
)
dfM$z1 <- z1$am

saveRDS(z1, file.path(cache.path, "sick_sensor_calibration_2023-07_upstream.RDS"))
```

```{r, include = FALSE}
# Niveau aval
z2 <- level_sensor_calibration(
  dfM,
  names(dfM)[grep("Niveau.aval.h2-", names(dfM), fixed = TRUE)],
  "Niveau.aval.h2.règle",
  "heure"
)
dfM$z2 <- z2$am
saveRDS(z2, file.path(cache.path, "sick_sensor_calibration_2023-07_downstream.RDS"))
```

```{r}
## Mesures effectuées les 27-28 novembre 2023 avec le seuil à 135°

dfC2 <- readr::read_tsv(
  file.path(input.path, "20231128 etalonnage sondes US.tsv")
)
```


```{r}
# Niveau amont
dfC2up <- dfC2[dfC2$`Localisation capteurs` == "amont", ]
z1 <- level_sensor_calibration(
  dfC2up,
  names(dfC2)[grep("Sonde", names(dfC2), fixed = TRUE)],
  "Hauteur mesurée (mm)"
)
dfC2up$z1 <- z1$am

saveRDS(z1, file.path(cache.path, "sick_sensor_calibration_2023-11_upstream.RDS"))
```

```{r}
# Niveau aval
dfC2down <- dfC2[dfC2$`Localisation capteurs` == "amont", ]
z2 <- level_sensor_calibration(
  dfC2down,
  names(dfC2)[grep("Sonde", names(dfC2), fixed = TRUE)],
  "Hauteur mesurée (mm)"
)
dfC2down$z2 <- z2$am

saveRDS(z2, file.path(cache.path, "sick_sensor_calibration_2023-11_downstream.RDS"))
```

```{r}
predictZ <- function(list_lm, v) {
  l <- lapply(list_lm, function(lm) {
    class(lm) <- "lm"
    predict(lm, data.frame(sensor_corrected = v))
  })
  m <- do.call(cbind, l)
  rowMeans(m)
}
df135$z1 <- predictZ(z1$lm[[1]], df135$`Niveau.amont.h1.(unit.=.mm.)-1`)
df135$z2 <- predictZ(z1$lm[[1]], df135$`Niveau.aval.h2-1`)
```

```{r}
## Mesures du 24 juillet 2024 avec le seuil à 135°
# Niveau amont
z1 <- level_sensor_calibration(df2407,
  names(df2407)[grep("Sick.amont", names(df2407), fixed = TRUE)],
  "Règle.amont",
  "Heure"
)

df2407$z1 <- z1$am

saveRDS(z1, file.path(cache.path, "sick_sensor_calibration_2024-07_upstream.RDS"))
```

```{r}
# Niveau aval
z2 <- level_sensor_calibration(df2407,
  names(df2407)[grep("Sick.aval", names(df2407), fixed = TRUE)],
  "Règle.aval",
  "Heure"
)
df2407$z2 <- z2$am

saveRDS(z2, file.path(cache.path, "sick_sensor_calibration_2024-07_downstream.RDS"))
```


```{r}
# Fusion des mesures

dfM$device_angle <- 90
df135$device_angle <- 135
df2407$device_angle <- 135
dfAll <- rbind.all.columns(dfM, df135)
dfAll <- rbind.all.columns(dfAll, df2407)
dfAll$exp_num <- seq(nrow(dfAll))
dfAll$Qobs <- dfAll$Qm3h / 3600
dfAll$z1 <- dfAll$z1 / 1000
dfAll$z2 <- dfAll$z2 / 1000
dfAll$device <- paste0("D", dfAll$device_angle)
l <- lapply(setNames(nm = names(cfg$devices[[1]])), function(x) {
  unlist(sapply(dfAll$device, function(r) cfg$devices[[r]][[x]]))
})
dfAll <- cbind(dfAll, do.call(cbind, l))
dfAll$h1 <- dfAll$z1 - dfAll$ZDV
dfAll$h2 <- dfAll$z2 - dfAll$ZDV
dfAll$h2 <- sapply(dfAll$h2, max, 0)
dfAll$submergence <- dfAll$h2 / dfAll$h1
dfAll$submergence[dfAll$submergence == 0] = NA
```

```{r}
z2oz1 <- dfAll$z2 / dfAll$z1
dfAll$validite[dfAll$exp_num %in% c(119, 142, 295, 235, 125, 140, which(z2oz1 > 1))] <- FALSE # Mesure visiblement erronée
```

```{r}
readr::write_tsv(dfAll, file.path(output.path, "consolidated_data.tsv"))
```

L'ensemble des mesures effectuées sont représentées Figure \@ref(fig:ObsAll).
Les numéros représentent le numéro de l'observation. Au total, `r nrow(dfAll)`
mesures ont été effectuées parmi lesquelles `r nrow(dfAll |> filter(validite))` 
mesures sont effectivement utilisées pour l'étude après élimination des mesures
qui paraissaient aberrantes.

```{r ObsAll,  out.width = "90%", fig.width=getFigWidth("90%"), fig.cap="Mesures des débits et de l'ennoiement en fonction de la charge amont pour les modèles de seuil D90 et D135"}
p <- plot_Q_fn_h1(dfAll %>% filter(validite), TRUE) + 
  geom_point(data = data.frame(x = -10, value = "Ecoulement dénoyé"), aes(x = x, y = x, fill = NULL, shape = NULL, col = as.factor(value)), 
             fill = "black", shape = 22, size = 5, stroke = NA) +
  scale_color_manual(values = "black")

p + geom_text_repel(aes(y = Qobs, label = exp_num), size = 2, , box.padding = 0.1)
```

```{r, eval = FALSE, include = FALSE}
Qm3h <- read_tsv(file.path(input.path, "Qm3h.tsv"))
Qm3s <- Qm3h %>% mutate(value = value / 3600)
plotQm3sTS <- function(Qm3s) {
  Qm3s$date <- as.Date(Qm3s$dt)
  ggplot(Qm3s, aes(x = dt, y = value)) +
    geom_line() +
    facet_grid(~date, scales = "free") +
    ggtitle("Measured flows") +
    labs(y = bquote("Flow rate " ~(m^3/s)))
}
plot(plotQm3sTS(Qm3s))

### Extraction des débits pour chaque expérience

width <- 60
l <- lapply(seq(nrow(df_events)), function(i) {
  iExp <- which(Qm3s$dt > df_events$date_start[i] & Qm3s$dt < df_events$date_end[i])
  if (length(iExp) > width) {
    dfe <- Qm3s[iExp, ]
    sd <- roll::roll_sd(dfe$value, width = width)
    j <- which.min(sd)
    data.frame(mean = roll::roll_mean(dfe$value, width = width)[j],
         sd = sd[j],
         start = i + j - round(width / 2),
         end = i + j + round(width / 2))
  } else {
    data.frame(mean = df_events$Q[i],
         sd = NA,
         start = NA,
         end = NA)
  }
})
df_evQ <- cbind(df_events, do.call(rbind, l))
```





