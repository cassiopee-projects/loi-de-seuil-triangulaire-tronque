# Lois de débit du seuil triangulaire tronqué

```{r, include = FALSE}
source("setup.R")
```

La loi de débit s'établit à partir du régime dénoyé. La littérature propose plusieurs
formules pour ce seuil composé auxquelles nous combinerons un coefficient de réduction
du débit en régime noyé pour lequel plusieurs possibilités existent aussi.

## Lois de débit en écoulement dénoyé

### Formulation pour un écoulement dans la partie triangulaire uniquement

Pour les hauteurs d'eau ne dépassant pas la partie triangulaire, la loi de débit $Q_T$
correspond à la formulation classique du seuil triangulaire telle que proposée par
@bosDischargeMeasurementStructures1989 (Équation&nbsp;\@ref(eq:Bos-triangle)).

\begin{equation}
  Q_T = C_t\ C_v\ \dfrac{16}{25} \sqrt{\dfrac{2}{5} g}\ \tan \dfrac{\theta}{2}\ h_1^{2.5}
  (\#eq:Bos-triangle)
\end{equation}

Avec les variables suivantes (cf. Figure&nbsp;\@ref(fig:Bos-section))&nbsp;:

- $C_t$ le coefficient de débit pour la loi de débit du seuil triangulaire
- $C_v$ le coefficient de vitesse d'approche
- $g$ l'accélération de la pesanteur (9.81 m.s<sup>-2</sup>)
- $\theta$ l'angle d'ouverture de la base du triangle
- $h_1$ la hauteur d'eau au dessus de la base du seuil

La formulation actuelle de Cassiopée issue de la documentation du logiciel DEVER
[@larinierProgrammeDEVER1993] néglige l'effet de la vitesse d'approche ($C_v=1$)
et contracte en une seule constante $C_1$ les autres coefficients
de l'équation pour donner l’Équation&nbsp;\@ref(eq:Cassiopee-triangle)&nbsp;:

\begin{equation}
  Q_T = C_1\ \tan \dfrac{\theta}{2}\ h_1^{2.5}
  (\#eq:Cassiopee-triangle)
\end{equation}

Avec des valeurs standards de $C_1$ égales à

- $1.37$ pour un déversoir en mince paroi
- $1.27$ pour un déversoir épais sans contraction
- $1.68$ et $1.56$ respectivement pour un déversoir à profil longitudinal triangulaire 1/2
amont et 1/2 ou 1/5 aval

La valeur du déversoir épais correspond au terme $\dfrac{16}{25} \sqrt{\dfrac{2}{5} g} = 1.27$
de l'Équation&nbsp;\@ref(eq:Bos-triangle).

Pour les hauteurs d'eau au-delà de la limite du triangle, plusieurs formulations
sont possibles.

### Formulation de Bos pour le seuil à crête mince

@bosDischargeMeasurementStructures1989 indique que la formule peut être obtenue
en soustrayant la loi de débit d'une section de contrôle triangulaire avec une
charge $(h_1 - H_B)$ de la loi de débit d'une section de contrôle triangulaire
de charge $h_1$.
Il justifie ce choix en indiquant que, pour les seuils à paroi mince, la surimposition ou
la soustraction de loi de débits de partie de la section de contrôle est
autorisée à condition que chaque partie concernée contienne une surface libre.

Cette formulation correspond à celle présente dans l'outil "Dever" de la version
"historique" de Cassiopée (cf. Équation&nbsp;\@ref(eq:Bos-sharp) et les notations
de la Figure&nbsp;\@ref(fig:Bos-section)). Elle est applicable pour tout $h_1 > H_B$.

\begin{equation}
  Q_{scw} = C_1\ \tan \dfrac{\theta}{2}\ \left [ h_1^{2.5} - (h_1 - H_B)^{2.5}  \right ]
  (\#eq:Bos-sharp)
\end{equation}


### Formulation de Bos pour le seuil à crête épaisse {#ch-loi-Bos-epais}

L'Équation&nbsp;\@ref(eq:Bos-broad) est dérivée du calcul de la hauteur critique dans la section de contrôle
et de l'égalité des charges entre la section de mesure amont et la section où
la hauteur critique est atteinte :

\begin{equation}
  Q_{bcw} = C_r\ C_v\ B_c\ \dfrac{2}{3}\ \sqrt{\dfrac{2}{3} g}\ \left ( h_1 - \dfrac{H_B}{2}  \right )^{1.5}
  (\#eq:Bos-broad)
\end{equation}

Avec $C_r$ le coefficient de débit du seuil rectangulaire et $B_c = 2H_B \tan \dfrac{\theta}{2}$ la largeur de la partie
rectangulaire du seuil.

A l'emplacement du déversement, la hauteur de l'eau est supposée être à la hauteur critique
comme illustré à la Figure&nbsp;\@ref(fig:Bos-critical-depth). De ce fait, la transition entre
l'équation triangulaire et l'équation triangulaire tronquée intervient lorsque
la hauteur critique est égale à $H_B$ soit lorsque $h1 = \frac{5}{4} H_B$ si on considère la
hauteur critique d'une section triangulaire.

```{r Bos-critical-depth, fig.cap="Schéma du profil longitudinal d'un seuil épais (extrait de Bos, 1989)"}
knitr::include_graphics("img/Bos_weir_longitudinal_profile.png")
```

Cette formulation introduit un second coefficient de débit $C_r$ qu'il est cependant possible
de calculer en fonction de $C_t$ en assurant la continuité du calcul du débit entre la partie triangulaire
et la partie rectangulaire lorsque $h_1 = H_B$.
En combinant les équations \@ref(eq:Bos-triangle) et \@ref(eq:Bos-broad) dans le cas où on considère
que les coefficient correcteurs $C_t$ et $C_r$ sont tous les deux égaux ainsi que les coefficients
d'approche $C_v$, et si on pose $C_1 = \dfrac{16}{25} \sqrt{\dfrac{2}{5} g}$, et
$C_2 = \dfrac{2}{3}\ \sqrt{\dfrac{2}{3} g}$, on a alors&nbsp;:

\begin{equation}
\begin{aligned}
  Q_T(5/4H_B) = Q_{bcw}(5/4H_B) \\
  C_1 \tan \dfrac{\theta}{2}\ H_B^{2.5} =
  B_c\ C_2\ \left ( \dfrac{H_B}{2}  \right )^{1.5} \\
  C_2 = \dfrac{C_1  \tan \dfrac{\theta}{2}\ H_B^{2.5}}{ B_c\ \dfrac{H_B^{1.5}}{2\sqrt{2}}} \\
  C_2 = \dfrac{2\sqrt{2}C_1}{B_c} \tan \dfrac{\theta}{2}H_B
  = \dfrac{2\sqrt{2}C_1}{B_c} \dfrac{B_c}{2} = \sqrt{2}C_1
\end{aligned}
(\#eq:C2)
\end{equation}

Ce qui finalement revient à l'équation :

\begin{equation}
  Q_{bcw} = \sqrt{2}C_1 B_c\ \left ( h_1 - \dfrac{H_B}{2}  \right )^{1.5}
  (\#eq:Bos-broad-C1)
\end{equation}

### Proposition de formule liée à la réduction de la section mouillée

La formule proposée ici correspond à la formulation du seuil triangulaire à
laquelle on applique un coefficient de réduction de débit égal à la réduction
de la surface mouillée liée à la partie rectangulaire (Équation&nbsp;\@ref(eq:Belaud-WetArea))

\begin{equation}
  Q_{war} = K_r\ C_1\ \tan \dfrac{\theta}{2}\ h_1^{2.5}
  (\#eq:Belaud-WetArea)
\end{equation}

Avec $K_r$ le coefficient de réduction de débit défini par la réduction de la surface
mouillée de la section triangulaire tronquée par rapport à la section triangulaire

\begin{equation}
  k_r = \dfrac{A_{tt}}{A_t} =
  \dfrac{h_1^2 \tan \dfrac{\theta}{2}}{2 H_B \tan \dfrac{\theta}{2} \left(h_1 - H_B / 2  \right)}
  (\#eq:Belaud-WetArea-Kr)
\end{equation}

Ce qui après simplification revient à l'Équation&nbsp;\@ref(eq:Belaud-Kr).

\begin{equation}
  K_r  = 2 \dfrac{H_B}{h_1} - \dfrac{H_B^2}{h_1^2}
  (\#eq:Belaud-Kr)
\end{equation}

## Transition entre la loi de débit triangulaire et triangulaire tronqué {#ch:transition}

La loi de débit de ce seuil composé faisant intervenir deux équations en fonction
de la hauteur d'eau amont, il est nécessaire de définir quelle cote de l'eau amont
constituera le point de bascule entre les deux lois.
Pour un seuil à crête épaisse, la hauteur critique étant atteinte à l'intérieur du seuil
(cf. Figure&nbsp;\@ref(fig:Bos-critical-depth)), le passage entre la forme triangulaire
et la forme rectangulaire s'effectue à $h1 = 5/4 H_B$
(cf. Section&nbsp;\@ref(ch-loi-Bos-epais)).
Pour un seuil à crête mince, la relation entre la hauteur d'eau amont et la hauteur
d'eau à l'emplacement du seuil est moins évidente comme on peut le voir sur le schéma (a) de
la Figure&nbsp;\@ref(fig:abbaspour-truncated-triangular-weir) (baisse de la hauteur d'eau "draw down").

L'utilisation de deux lois différentes peut aussi poser le problème de la continuité des débits
lors du passage d'une loi à l'autre comme évoqué à la Section&nbsp;\@ref(ch-loi-Bos-epais).

Pour pallier ces deux problèmes, nous proposons une formulation générale (Équation&nbsp;\@ref(eq:Qtransition))
assurant une pondération progressive $a$ entre la loi de débit triangulaire $Q_T$ et
la loi de débit triangulaire rectangulaire $Q_R$ déclenchée entre des seuils $H_{t1} = k_{t1} H_B$
et $H_{t2} = k_{t2} H_B$ à déterminer expérimentalement.

\begin{equation}
  Q_1 = (1 - a) Q_T + a\ k\ Q_R
  (\#eq:Qtransition)
\end{equation}

Avec $k$ le coefficient d'ajustement de la loi rectangulaire (théoriquement
égale à 1), et $a$ calculé selon l'arbre de décision Figure&nbsp;\@ref(fig:decision-a) ou l’Équation&nbsp;\@ref(eq:a) après simplification.

```{r decision-a, fig.cap="Arbre de décision du calcul du coefficient de pondération des lois de débit"}
mmd <- '
flowchart LR
    Q1{h1 < Ht1}
    Q1 -- oui --> Q2{h1 > Ht2}
    Q1 -- non --> Qt[a = 0]
    Q2 -- oui --> Qtt[a = 1]
    Q2 -- non --> Qm["a = (h1 - Ht1)/(Ht2 - Ht1)"]
'

knitr::include_graphics(fairify::mermaid(mmd, dir.dest = cache.path))
```

\begin{equation}
  a = \max\left(0, \min\left(1, \dfrac{h_1 / H_B - k_{t1}}{k_{t2} - k_{t1}}\right)\right)
  (\#eq:a)
\end{equation}

## Lois de débit pour l'écoulement noyé

### Formulation générale

L'écoulement noyé (ou submergé) se caractérise par une limitation du débit entraînée
par une augmentation de la cote de l'eau $h_2$ à l'aval du seuil
(Figure&nbsp;\@ref(fig:fig-villemonte)). L'écoulement noyé se caractérise aussi
à débit constant par une variation de la hauteur d'eau amont lorsque la hauteur
d'eau aval varie.

Plusieurs méthodes existent pour estimer le débit d'un seuil submergé à partir du
coefficient d'ennoiement $h_2/h_1$&nbsp;:

- la méthode de @villemonteSubmergedWeirDischarge1947 ou de @wuSubmergedFlowRegimes1996
qui consiste à appliquer un coefficient de réduction appliqué sur la loi de débit
dénoyée $Q_1$ (Cf. Équation&nbsp;\@ref(eq:Qsubmerged))
- la méthode de Wessels (Cf. @cantoRatingCompoundSharpcrested2002 p. 24)
qui corrige $h_1$ pour obtenir un $h_1$ équivalent en régime dénoyé

\begin{equation}
  Q_s(h_1, h_2) = C_s(h_2/h_1) Q_1(h_1)
  (\#eq:Qsubmerged)
\end{equation}

@wuSubmergedFlowRegimes1996 propose un coefficient de réduction ajusté sur les mesures
effectuées sur des seuils rectangulaires non contracté de différentes hauteurs
(Équation&nbsp;\@ref(eq:CsWu) et Figure&nbsp;\@ref(fig:Wu-submerged-reduction-factor)).
Contrairement à la formule de Villemonte, cette méthode n'a été testée que pour le
seuil rectangulaire non contracté et ne sera pas retenue pour cette étude.

\begin{equation}
  C_s = 1.0 + 1.162(h_2/h_1) - 1.331 \sin^-1 (h_2/h_1)
  (\#eq:CsWu)
\end{equation}

```{r Wu-submerged-reduction-factor, fig.cap="Evolution du coefficient de réduction de débit en fonction de la submergence (extrait de Wu et Rajaratnam, 1996)"}
knitr::include_graphics("img/Wu_submerged_flow_reduction_factor.png")
```

La méthode de Wessels n'ayant pas été utilisée en dehors de quelques travaux du même
auteur et de son équipe [@wesselsFlowgaugingStructuresSouth2009], nous ne la testerons
pas ici. Pour plus de détail sur cette méthode et ses résultats expérimentaux,
se reporter à @cantoRatingCompoundSharpcrested2002 p. 24 et suivante.

### Formulation de Villemonte

La formule de @villemonteSubmergedWeirDischarge1947 est la formule qui était et
est implémentée dans les différentes versions de Cassiopée pour les deversoirs
rectangulaires et triangulaires noyés et est aussi utilisée par ailleurs pour le
dimensionnement des passes à bassins à fentes verticales et les prébarrages
[@fuentes-perezVillemontesApproachGeneral2017].

L'hypothèse de Villemonte repose sur l'idée
que le débit noyé $Q$ correspond à la différence entre le débit dénoyé $Q_1$
résultant de $h_1$ et le débit opposé $Q_2$ résultant de $h_2$
(Figure&nbsp;\@ref(fig:fig-villemonte) et Équation&nbsp;\@ref(eq:villemonte-Q).

```{r fig-villemonte, fig.cap="Schéma de l'hypothèse de Villemonte (extrait de Fuentes-Pérez et al. (2017)"}
knitr::include_graphics("img/Villemonte_scheme.png")
```

\begin{equation}
  Q = Q_1 - Q_2 \implies \dfrac{Q}{Q_1} = 1 - \dfrac{Q_2}{Q_1}
  (\#eq:villemonte-Q)
\end{equation}

Le coefficient de réduction du débit $C_s$ s'exprime alors avec l'Équation&nbsp;\@ref(eq:villemonte-Cs).

\begin{equation}
  C_s = \dfrac{Q}{Q_1} = k \left [ 1 - \left (\dfrac{h_2}{h_1} \right ) ^n \right ]^m
  (\#eq:villemonte-Cs)
\end{equation}

$k$ et $m$ sont des constantes déterminées expérimentalement, et $n$ est l'exposant
de l'expression ${h_1}^n$ utilisée dans la loi de débit dénoyée.

Les expérimentations de @villemonteSubmergedWeirDischarge1947 portant sur les seuils à
crête mince de forme diverses (proportionnelle, rectangulaire contracté ou non
contracté, parabolique, triangulaire (90°) et parabolique) ont conclu sur des
valeurs de $k=1$ et $m=0.385$ pour des niveaux d'ennoiement allant de 0 à
90&nbsp;% avec une marge d'erreur inférieure à 5&nbsp;%.

@villemonteSubmergedWeirDischarge1947 propose finalement le coefficient d'ennoiement
de l'Équation&nbsp;\@ref(eq:villemonte-Cs2).

\begin{equation}
  C_s = \left [ 1 - \left (\dfrac{h_2}{h_1} \right ) ^n \right ]^{0.385}
  (\#eq:villemonte-Cs2)
\end{equation}

### Prise en compte du seuil composé par la formule de Villemonte

L'hypothèse de Villemonte étant basée sur la différence entre le débit dénoyé $Q_1$
résultant de $h_1$ et le débit opposé $Q_2$ résultant de $h_2$
(Cf. Figure&nbsp;\@ref(fig:fig-villemonte)), l'utilisation de deux lois de débit
qui peuvent être différentes en fonction de la hauteur d'eau amont $h_1$ ou aval
$h2$ nécessite d'utiliser la formulation générale de l’Équation&nbsp;\@ref(eq:villemonte-Cs):

\begin{equation}
  Q = Q_1 \left [ 1 - \dfrac{Q_2}{Q_1} \right ]^m
  (\#eq:villemonte-Qgen)
\end{equation}

Les équations de lois de débit dénoyées \@ref(eq:Bos-triangle), \@ref(eq:Bos-sharp),
\@ref(eq:Bos-broad), \@ref(eq:Belaud-WetArea) de même que la combinaison linéaire
de ces lois proposée dans l’Équation&nbsp;\@ref(eq:Qtransition) peuvent alors être utilisées
pour calculer $Q_1$ et $Q_2$ et calculer le débit noyé grâce à l’Équation
\@ref(eq:villemonte-Qgen).

## Formulation de Villemonte modifiée

La formulation de Villemonte pour les seuils considère que la réduction de débit
commence dès que la cote de l'eau aval atteint la base de la crête du seuil.
La réalité physique est plus complexe.
Sur les seuils à crête épaisse, l'effet de l'ennoiement sur le coefficient de
réduction du débit n'intervient qu'à partir du moment où la cote aval atteint
la hauteur critique au dessus du seuil.
On peut aussi observer une influence de l'aval à une cote inférieure à celle de la
base de la crête dans le cas d'un écoulement noyé en dessous (Voir photo
Figure \@ref(fig:photo-D135-noye-dessous)), à ressaut éloigné ou
à ressaut recouvrant le pied de nappe avec une possible augmentation du débit
(cf. @cetmefNoticeDeversoirsSynthese2005 p.&nbsp;47-48).

```{r photo-D135-noye-dessous, out.width="50%",fig.cap="Photos de profil de l'aval du seuil D135 noyé en dessous"}
knitr::include_graphics("img/photo_seuil_noye_par_dessous.jpg")
```

Le coefficient de réduction de débit des expériences de @vennard1943submergence
montre ce phénomène avec un débit ne commençant à diminuer qu'à partir d'un
coefficient d'ennoiement $h_2/h_1$ supérieur à 0.4 pour des hauteurs de pelle faibles
par rapport à la charge amont (Voir rapport $H/P$ dans la Figure&nbsp;
\@ref(fig:Vennard-submerged-reduction-factor) extraite de @hulsingUSGSTechniquesWaterResources1967).

```{r Vennard-submerged-reduction-factor, fig.cap="Evolution du coefficient de réduction de débit en fonction de la submergence (extrait de Hulsing, 1967)"}
knitr::include_graphics("img/Vennard_submerged_flow_reduction_factor.png")
```

Nous proposons ici de prendre en compte le décalage de l'effet de l'ennoiement sur
le coefficient de réduction du débit via la formule suivante qui est démontrée
au paragraphe \@ref(ch:resultat-villemonte-modif)&nbsp;:

\begin{equation}
  Q = Q_1 \min\left(1, \left [ 1 - \dfrac{Q_2}{Q_1} \right ]^m \times k_I\right)
  (\#eq:villemonte-Qgen-modif)
\end{equation}

## Bilan des équations testées et de leurs paramètres respectifs

Chacune des lois testées utilisera la loi de transition permettant d'ajuster
le passage entre la section triangulaire et la section triangulaire tronquée
(Équation&nbsp;\@ref(eq:Qtransition)) pour les calculs dénoyés $Q_1$ et $Q_2$
et la formulation générale de Villemonte (Équation&nbsp;\@ref(eq:villemonte-Qgen))
sera utilisée pour le calcul du débit en régime noyé.
Ces trois équations nécessitent la calibration des paramètres: $k_{t1}$ et
$k_{t2}$ de limite de transition de forme de section et l'exposant $m$ de la 
formule générale de Villemonte théoriquement égal à  0.385.

Chacune des lois utilisera l'Équation&nbsp;\@ref(eq:Cassiopee-triangle) pour la
section triangulaire et cela nécessite l'ajustement du coefficient $C_1$.

Les lois pour la section triangulaire tronquée sont résumées dans le
Tableau ci-dessous.
Elles nécessitent toutes le calage du coefficient de débit $C_1$, du coefficient
d'ajustement $k$ introduit dans \@ref(eq:Qtransition) et des coefficients de limite
de transition $k_{t1}$ et $k_{t2}$ utilisés dans l'Équation&nbsp;\@ref(eq:a).

Table: (\#tab:liste-equations) Liste des équations testées dans l'étude

| Nom de l'équation | Équation numéro |
| ----------------- | --------------- |
| Bos à crête mince (*scw*) | \@ref(eq:Bos-sharp) |
| Bos à crête épaisse (*bcw*) | \@ref(eq:Bos-broad) |
| Réduction section mouillée (*war*) | \@ref(eq:Belaud-Kr) |

L'optimisation de ces paramètres consiste à la minimisation du critère NRMSE
(Racine carrée de la somme des écarts de débit observé et simulé au carré
normalisé par la moyenne des débits observés (Équation&nbsp;\@ref(eq:nrmse)).

\begin{equation}
  NRMSE = \dfrac{\sqrt{\dfrac{1}{n} \sum^n_i \left(Q_{sim} - Q_{obs} \right)^2}}{\overline{Q_{obs}}}
  (\#eq:nrmse)
\end{equation}
